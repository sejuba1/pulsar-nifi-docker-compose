from pulsar import Function
from json2xml import json2xml
from json2xml.utils import readfromurl, readfromstring, readfromjson

class ConvertJsonToXmlFunction(Function):
  def __init__(self):
    pass

  def process(self, input, context):
    data = readfromstring(input)
    return json2xml.Json2xml(data).to_xml()
